#include "cmn.h"

static void Test_SQLBrowseConnect(void);
static void Test_SQLGetDescRec(void);

static void
Test_SQLBrowseConnect(void)
{
    SQLRETURN ret;
    SQLHENV env;
    SQLHDBC conn;
    SQLCHAR str[1024] = {0};
    SQLSMALLINT strl;

    SQLAllocHandle(SQL_HANDLE_ENV, SQL_NULL_HANDLE, &env);
    SQLSetEnvAttr(env, SQL_ATTR_ODBC_VERSION, (void *)SQL_OV_ODBC3, 0);
    SQLAllocHandle(SQL_HANDLE_DBC, env, &conn);

    ret = SQLBrowseConnect(conn, (SQLCHAR *)"DSN=odbctest",
                           SQL_NTS, str, 1024, &strl);
    if (SQL_SUCCEEDED(ret))
    {
        printf("success:%s\nlen:%d\n\n", str, strl);
    }
    else
    {
        printf("failed!\n");
    }
}

static void
Test_SQLGetDescRec(void)
{
    SQLRETURN ret;
    HSTMT hstmt = SQL_NULL_HSTMT;
    SQLHENV env;
    SQLHDBC conn;
    SQLCHAR str[1024] = {0};
    SQLSMALLINT strl;
    SQLHDESC hdesc = NULL;
    SQLCHAR name[64];
    SQLINTEGER strLen = 0;
    SQLSMALLINT rType = 0;
    SQLSMALLINT rsubType = 0;
    SQLLEN len = 0;
    SQLSMALLINT rPrecision = 0;
    SQLSMALLINT rScale = 0;
    SQLSMALLINT rNullable = 0;
    SQLCHAR sqlstate[16];
    SQLINTEGER fNativeError;
    SQLCHAR szErrorMsg[256];
    SQLSMALLINT cbErrorMsg;

    SQLAllocHandle(SQL_HANDLE_ENV, SQL_NULL_HANDLE, &env);
    SQLSetEnvAttr(env, SQL_ATTR_ODBC_VERSION, (void *)SQL_OV_ODBC3, 0);
    SQLAllocHandle(SQL_HANDLE_DBC, env, &conn);

    ret = SQLBrowseConnect(conn, (SQLCHAR *)"DSN=odbctest",
                           SQL_NTS, str, 1024, &strl);
    if (!SQL_SUCCEEDED(ret))
    {
        printf("failed!\n");
    }

    ret = SQLAllocHandle(SQL_HANDLE_STMT, conn, &hstmt);

    ret = SQLExecDirect(hstmt, (SQLCHAR *)"select * from test", SQL_NTS);

    ret = SQLGetStmtAttr(hstmt, SQL_ATTR_IMP_ROW_DESC, &hdesc, 0, NULL);

    if (hdesc)
        ret = SQLGetDescRec(hdesc, 1, name, sizeof(name), &strLen, &rType,
                            &rsubType, &len, &rPrecision, &rScale, &rNullable);

    if (!SQL_SUCCEEDED(ret))
    {
        SQLGetDiagRec(SQL_HANDLE_STMT, hstmt, 1, sqlstate, &fNativeError,
                      szErrorMsg, SQL_MAX_MESSAGE_LENGTH, cbErrorMsg);
        printf("ERROR:%s:%s\n", sqlstate, szErrorMsg);
    }
    else
    {
        printf("name:%s\n", name);
        printf("strlen:%d\n", strLen);
        printf("type:%d\n", rType);
        printf("subType:%d\n", rsubType);
        printf("len:%d\n", len);
        printf("precision:%d\n", rPrecision);
        printf("scale:%d\n", rScale);
        printf("nullable:%d\n", rNullable);
    }
}

int main()
{
    Test_SQLBrowseConnect();
    Test_SQLGetDescRec();
}
