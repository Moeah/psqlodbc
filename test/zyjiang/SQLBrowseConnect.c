#include "cmn.h"

static void Test_SQLBrowseConnect(void);

static void Test_SQLBrowseConnect(void)
{
    SQLRETURN ret;
    SQLHENV env;
    SQLHDBC conn;
    SQLCHAR str[1024];
    SQLSMALLINT strl;

    SQLAllocHandle(SQL_HANDLE_ENV, SQL_NULL_HANDLE, &env);
    SQLSetEnvAttr(env, SQL_ATTR_ODBC_VERSION, (void *)SQL_OV_ODBC3, 0);
    SQLAllocHandle(SQL_HANDLE_DBC, env, &conn);

    ret = SQLBrowseConnect(conn, (SQLCHAR *)"DSN=psqlodbc_test_dsn",
                           SQL_NTS, str, 1024, &strl);
    if (SQL_SUCCEEDED(ret))
    {
        printf("success:%s\nlen:%d\n", str, strl);
    }
    else
    {
        printf("failed!\n");
    }
}
int main()
{
    Test_SQLBrowseConnect();
}